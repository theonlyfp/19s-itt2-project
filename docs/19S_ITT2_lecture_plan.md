---
title: '19S ITT2 Project'
subtitle: 'Lecture plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>', 'Morten Bo Nielsen \<mon@ucl.dk\>']
main_author: 'Morten Bo Nielsen'
date: \today
email: 'mon@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait18einB, 19S
* Name of lecturer and date of filling in form: NISI, MON 2019-01-09
* Title of the course, module or project and ECTS: Semester project, 10 ECTS
* Required readings, literature or technical instructions and other background material: Good understanding of all topic covered in ITT1 is expected.

See weekly plan for details like detailed daily plan, links, references, exercises and so on.


-------- ------ ---------------------------------------------
 INIT     Week  Content
-------- ------ ---------------------------------------------
MON/NISI  5     Project part 1

                Introducing the course and the projects.

                Forming teams. Introducing gitlab for project management.

                Phase 1: Building a Proof-of-concept system

                Building the minimal system: Button-Atmel-RPi-Network-Server in Juniper lab.

                Schematic + Blockdiagram

                Forming groups

                Gitlab workflow

                Atmel studio install

MON/NISI  6     Continue work on minimal system.

MON/NISI  7     Continue work on minimal system.

MON/NISI  8     Phase 2: consolidating

                ADC value to temp algorithm.

                Raspberry API client and Server API update to be able to handle temperature readings.

                Each group install a virtual server in a cloud.

MON/NISI  9     Continue analog sensor, API and virtual server

MON/NISI  10    Continue analog sensor, API and virtual server

MON/NISI  11     Phase 3: Make it useful for the user

                PWM LED controller extension.

                Raspberry API client and Server API update to be able to handle PWM LED control output.

                Status LED extension for UART traffic

MON/NISI  12    Fablab Workshops + regular project work

MON/NISI  13    Fablab Workshops + regular project work

MON/NISI  14    Idem

MON/NISI  15    Fontys sensor network

MON/NISI  16    Easter break

MON/NISI  17    Project part 2

                Design your own system, og follow the teachers template project.

                Generic ideas:

                * Hardware status LED for UART traffic

                * Extra sensors [like this one](https://www.bosch-sensortec.com/bst/products/all_products/bme280)?

                * monitoring of services

                * automated build and test on hardware

                * I2C

                * webservice connecting to REST API

                * Move from school system to cloud service, e.g. AWS

                * Integrate ESP

                Phase 1: Building a proof of concept system

MON/NISI  18    TBD by students and teachers

MON/NISI  19    TBD by students and teachers

MON/NISI  20    Phase 2: Consolidating

                TBD by students and teachers

MON/NISI  21    TBD by students and teachers

MON/NISI  22    TBD by students and teachers

MON/NISI  23    Phase 3: Make it useful for the user

                TBD by students and teachers

MON/NISI  24    TBD by students and teachers

MON/NISI  25    TBD by students and teachers

MON/NISI  26    Exams

---------------------------------------------------------


# General info about the course, module or project

## The student’s learning outcome

Learning goals for the course are described in the curriculum section 2 about the national elements.

Besides consolidating the material from the other course, a large part of the project is directly related to the topic about project management (section 2.4 in the curriculum).
See the curriculum for more details2

## Content

The course is formed around a project, and will include relevant technology from the curriculum. It is intended for cross-topic consolidation.

## Method

The project will be divided into blocks that build upon each other. The idea is to create a system that works from sensor to presentation in the cloud while the students practice project management and learn operational skills.

## Equipment

None specified at this time as it is dependent on the topic chosen.

## Projects with external collaborators  (proportion of students who participated)
None at this time.

## Test form/assessment
The project includes two compulsory elements.

A process presentation during the project and an end-of-project report, that documents the students learning and work during the semester.

See exam catalogue for details on compulsory elements.

## Other general information
None at this time.

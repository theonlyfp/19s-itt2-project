---
Week: 12
Content: Project part 1 phase 3
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 12 Make it useful for the user

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* all students have a personal gitlab.io page

### Learning goals

Gitlab ci

* level 1: The student know how CI works in gitlab and how to use it
* level 2: The student is able to set up a simple CI pipeline, e.g. using pages
* level 3: The student is able to set up and use pipelines in general

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)
    5. Any other business (AOB)

* A personal gitlab page
* Continue work on datalogger housing  

## Schedule

Monday

* 8:15 Introduction to the day, general Q/A session

* 8.30 Gitlab CI

    MON will go through gitlab CI model and pipelines.
    
    We will decide on common how to work with it - some of it will be on class.
    
    How long we are it will take, depends on you.

* 9:00 Fablab workshops (9:00 - 15:30)

    1 member from each team goes to fablab for laser cutter and 3D workshops

* 10:00? You work on deliverables

    Group morning meeting
    
        You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
        Ordinary agenda:
        1. (5 min) Round the table: What did I do, and what did I finish?
        2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
        3. (5 min) Round the table: Claim one task each.

    Remember to come ask questions if you have any.  

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting (See monday for agenda)

* 9:00 Teacher meetings

    Timeslot for you weekly 10 min. meeting with the teachers.

    Remember to book a time and have an agenda prepared.

* 9:00 Fablab workshops (9:00 - 15:30)

    1 member from each team goes to fablab for laser cutter and 3D workshops

* 9:00 You work on deliverables.

    Come ask us if you have questions.


## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.


## Comments

* Some from each group are going to fablab today and tomorrow
* We have discussed gitlab CI before. This time we will do more details.
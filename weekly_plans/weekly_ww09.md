---
Week: 09
Content: Project part 1 phase 2
Material: See links in weekly plan
Initials: MON/NISI
---

# Week 09 Consolidation

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* All groups have consolidated and completed their documentation
* All groups have updated the Server API and API client to handle temperature readings
* All groups have installed a virtual server in a cloud

### Learning goals
* Documentation and guides
  * level 1: The student knows what is relevant in relation to creating documentation for external stakeholders
  * level 1: The student is able to create documentation and guides for a specific purpose
  * level 3: Creating relevant documentation and guides are part of the normal workflow

## Deliverables
* Mandatory weeky meetings with the teachers (this includes minutes of the meeting)
  * Agenda is:
    1. Status on project (ie. show closed tasks in gitlab)
    2. Next steps (ie. show next tasks in gitlab)
    3. Collaboration within the group (ie. any internal issues, fairness of workload, communication)
    4. Help needed or offered (ie. what help do you need and where do you feel you can contribute to the class)

* Install a virtual server in a cloud
* Consolidate and complete documentation
* Update `group.md`

## Schedule

Monday

* 8:15 Introdution to the day, general Q/A session, exercises

* 8.45 Samuel Interrupts and timers in C

* 9:15 On creating documentation and guides

* 9:30 Group morning meeting

    You will meet in your groups every morning and decide on who does what. This is part of the project management, and is mandatory.
    Ordinary agenda:
    1. (5 min) Round the table: What did I do, and what did I finish?
    2. (5-10 min) Review of tasks: Are they still relevant? do we need to add new ones?
    3. (5 min) Round the table: Claim one task each.

* 10:00 You work on deliverables

    Remember to come ask questions if you have any.   

* 12:30 Q&A Session if needed

Tuesday

* 8:15 Introdution to the day, general Q/A session

* 8:30 Group morning meeting (See monday for agenda)

* 9:00 You work on deliverables.

    Come ask us if you have questions.

* 12:30 Information about elective courses voting procedure

## Hands-on time

See the [exercise document](https://eal-itt.gitlab.io/19s-itt2-project/19S_ITT2_exercises.pdf) for details.

## Comments

* Remember that project classes is moved in week 10. You have project 5th and 6th of March in week 10!

* For the `group.md`, see [here](https://gitlab.com/EAL-ITT/19s-itt2-project/blob/master/docs/grouplist/grouplist_prepend.md) for template
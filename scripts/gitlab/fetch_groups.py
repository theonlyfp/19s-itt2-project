#!/usr/bin/env python

import gitlab
import sys

groupfile='group.md'
default_groups_filename = 'groups.txt'

def get_token( filename='token.txt'):
	with open(filename) as f:
		token=f.readline()
	return token.strip()


if __name__ == "__main__":

	token = get_token()

	gl = gitlab.Gitlab('https://gitlab.com', private_token=token )
	gl.auth()

	if len(sys.argv) > 1:
		list_of_groups_filename = sys.argv[1]
	else:
		list_of_groups_filename = default_groups_filename

	with open(list_of_groups_filename) as gr_lst:

		for p_name in gr_lst.readlines():
			try:
				project = gl.projects.get(p_name.strip())

				f = project.files.get(file_path=groupfile, ref='master')

				# get the decoded content
				print(f.decode())

				print( "")
				print( "project path: [{}]({})".format( project.web_url, project.web_url ) )
				print( "")

			except gitlab.exceptions.GitlabGetError:
				print("Unknown group")
				print("----------------" )
				print("Unable to fetch file {} from {}".format(groupfile, p_name.strip() ) )
				print( "")
			except TypeError:
				#yes silently failing ...
				pass
